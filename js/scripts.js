'use strict'

const xhr = new XMLHttpRequest();
xhr.open('GET', 'https://randomuser.me/api/?results=5&seed=abc'); // настройки запроса
xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
        if (xhr.status == 200) { // связь с сервером установлена
            // console.log( JSON.parse(xhr.responseText));
            const data = JSON.parse(xhr.responseText);
            renderUsers(data.results);
        }
    }
}

xhr.send(); // запрос

function renderUsers(users) {
    const div = document.getElementById('users');
    div.innerHTML = '';
    for (let i = 0; i < users.length; i++) {
        console.log(users[i]);
        div.innerHTML += `
        <div class="user">
            <img class="user__logo" src="${users[i].picture.large}" alt="">
            <div class="user__info">
                <div class="user__info-name">${users[i].name.first} ${users[i].name.last}</div>
                <div class="user__info-age">Возраст: ${users[i].dob.age} лет</div>
            </div>
        </div>
        `;
    }
}

let openModal = document.getElementsByClassName('contact__button')[0];
openModal.addEventListener('click', showModal);

let closeModal = document.getElementsByClassName('modal__close')[0];
closeModal.addEventListener('click', hideModal);

function showModal() {
    let modalWrapper = document.getElementsByClassName('modal-wrapper')[0];
    modalWrapper.style.display = ('flex');
}

function hideModal() {
    let modalWrapper = document.getElementsByClassName('modal-wrapper')[0];
    modalWrapper.style.display = ('none');
}

